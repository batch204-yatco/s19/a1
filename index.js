// console.log("Hello World");

1
let username = prompt("Enter your username");

let password = prompt("Enter your password");

let role = prompt("Enter your role").toLowerCase();

if (username === ""){
	alert("Username input should not be empty");
} else if (username === null){
	alert("Username input should not be empty");
} else if (password === ""){
	alert("Password input should not be empty");
} else if (password === null){
	alert("Password input should not be empty");
} else if (role === ""){
	alert("Role input should not be empty");
} else if (role === null){
	alert("Role input should not be empty");
} else {
	switch(role){
		case "admin":
		console.log("Welcome back to the class portal, admin!");
		break;
		case "teacher":
		console.log("Welcome back to the class portal, teacher!");
		break;
		case "student":
		console.log("Welcome back to the class portal, student!");
		break;
		default:
		console.log("Role out of range.");
		break;
	}
}

// 2

function checkAverage(num1, num2, num3, num4){
	let average = Math.round((num1 + num2 + num3 + num4)/4);
	// console.log(average);
		// console.log("checkAverage(" + num1 + "," + num2 + "," + num3 + "," +num4 + ");");

	if(average <= 74){
		console.log("Hello, student, your average is " + average + " the letter equivalent is F");
	} else if(average >= 75 && average <= 79){
		console.log("Hello, student, your average is " + average + " the letter equivalent is D");
	} else if(average >= 80 && average <= 84){
		console.log("Hello, student, your average is " + average + " the letter equivalent is C");
	} else if(average >= 85 && average <= 89){
		console.log("Hello, student, your average is " + average + " the letter equivalent is B");
	} else if(average >= 90 && average <= 95){
		console.log("Hello, student, your average is " + average + " the letter equivalent is A");
	} else if(average >= 96){
		console.log("Hello, student, your average is " + average + " the letter equivalent is A+");
	}
}

checkAverage(70,70,72,71);
checkAverage(76,76,77,78);
checkAverage(81,83,84,85);
checkAverage(87,88,88,89);
checkAverage(91,90,92,90);
checkAverage(96,95,97,97);
